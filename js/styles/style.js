let logotype = document.querySelector(".logotype");

logotype.addEventListener('mouseover', function() {
	this.style.borderLeft = "4px #AFEF1F solid";
	this.style.borderRight = "4px #AFEF1F solid";

	this.children[1].style.borderLeft = "4px #53FDCD solid";
	this.children[1].style.borderRight = "4px #53FDCD solid";

	this.children[0].style.display = "none";
 	this.children[1].style.display = "block";
 	this.children[1].style.height = "102px";
})

logotype.addEventListener('mouseleave', function() {
	this.style.borderLeft = "none";
	this.style.borderRight = "none";

	this.children[0].style.borderLeft = "none";
	this.children[0].style.borderRight = "none";

	this.children[0].style.display = "block";
 	this.children[1].style.display = "none";
})

let btnWrapp = document.querySelector(".wrapper-button");

btnWrapp.addEventListener('mouseover', function() {
	this.style.borderLeft = "4px #AFEF1F solid";
	this.style.borderRight = "4px #AFEF1F solid";

	this.children[0].style.borderLeft = "4px #53FDCD solid";
	this.children[0].style.borderRight = "4px #53FDCD solid";

	this.children[0].style.width = "222px";
	this.children[0].style.color = "#AFEF1F";
})

btnWrapp.addEventListener('mouseleave', function() {
	this.style.borderLeft = "none";
	this.style.borderRight = "none";

	this.children[0].style.borderLeft = "none";
	this.children[0].style.borderRight = "none";

	this.children[0].style.width = "230px";
	this.children[0].style.color = "white";
})